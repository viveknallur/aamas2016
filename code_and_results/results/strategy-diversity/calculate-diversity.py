from __future__ import division
from collections import Counter
import csv
import math
import sys

TOTAL_POPULATION = 501

def calc_ginisimpson(pop):
    lamb_da = 0 # lambda is a reserved word in python
    specie_proportion = Counter(pop)
    #print("specie_proportion is: %s"%(specie_proportion))
    for k,v in specie_proportion.items():
        lamb_da = lamb_da + math.pow((v/TOTAL_POPULATION),2)
    #print("Lambda is: %f"%(lamb_da))
    print("Gini-Simpson index is: %f"%(1-lamb_da))

def calc_shannon(pop):
    H = 0
    specie_proportion = Counter(pop)
    for k,v in specie_proportion.items():
        #print("v = %d, total = %d"%(v, TOTAL_POPULATION))
        prop = v/TOTAL_POPULATION
        #print("Prop = %f"%(prop))
        H = H + (prop * math.log10(prop))
    H = -H
    print("Shannon is: %f"%(H))

def get_rows(filename):
    with open(filename) as csvfile:
        popreader = csv.reader(csvfile)
        while(True):
	        config = popreader.next()
                #print(config)

                # For RL plus Evo plus BestPlay
                reproCycle, memory, repro_proportion, RLProportion, recency, epsilon = config
                print("repCycle: %s, memory: %s, reproProp: %s, RLProp: %s, recency: %s, epsilon: %s"%(reproCycle, memory, repro_proportion, RLProportion, recency, epsilon)) 

                # For RL plus Best Play
                #recency, epsilon, memory, repro_proportion = config
                #print("memory: %s, reproProportion: %s, recency:  %s, epsilon: %s"%(memory, repro_proportion, recency, epsilon))

                # For Evo plus RL
#                reproFreq, memory, repro_proportion, recency, epsilon = config
#                print("RepCycle: %s, memory: %s, reproProportion: %s, recency:  %s, epsilon: %s"%(reproFreq, memory, repro_proportion, recency, epsilon))
                #########################

                # For Evo plus Best play
#                reproFreq, memory, repro_proportion = config
#                print("RepCycle: %s, memory: %s, reproProportion: \
#                        %s"%(reproFreq, memory, repro_proportion))
                ###########################

                for row in popreader.next():
	            population = row
	            population = population[1:]
	            population = population[:-1]
	            population = [float(i) for i in population.split()]
                    print("Population size: %d"%(TOTAL_POPULATION))
	            calc_ginisimpson(population)
	            calc_shannon(population)
	
if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("I need a data file")
        exit (1)
    else:
        filename = sys.argv[1]
        filepath = \
    '/home/vivek/bitbucket/aamas2016/code_and_results/results/strategy-diversity/'
        full_filename = ''.join([filepath, filename])
        population = get_rows(full_filename)
