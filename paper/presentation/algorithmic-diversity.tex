
\documentclass[xcolor=x11names,compress]{beamer}

%% General document %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{graphicx}
\usepackage{epstopdf}
\usepackage{tikz}
\usetikzlibrary{decorations.fractals}
\usepackage{mathtools}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\setbeamertemplate{navigation symbols}{}%remove navigation symbols

%% Beamer Layout %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\useoutertheme[subsection=false,shadow]{miniframes}
\useinnertheme{default}
\usefonttheme{serif}
\usepackage{palatino}

\setbeamerfont{title like}{shape=\scshape}
\setbeamerfont{frametitle}{shape=\scshape}

\setbeamercolor*{lower separation line head}{bg=DeepSkyBlue4} 
\setbeamercolor*{normal text}{fg=black,bg=white} 
\setbeamercolor*{alerted text}{fg=red} 
\setbeamercolor*{example text}{fg=black} 
\setbeamercolor*{structure}{fg=black} 
 
\setbeamercolor*{palette tertiary}{fg=black,bg=black!10} 
\setbeamercolor*{palette quaternary}{fg=black,bg=black!10} 

\renewcommand{\(}{\begin{columns}}
\renewcommand{\)}{\end{columns}}
\newcommand{\<}[1]{\begin{column}{#1}}
\renewcommand{\>}{\end{column}}

\newcommand\Mycomb[2][n]{\prescript{#1\mkern-0.5mu}{}C_{#2}}

\newcommand{\exedout}{%
  \rule{0.8\textwidth}{0.5\textwidth}%
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%




\begin{document}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\section{\scshape Introduction}
\begin{frame}
\title{Algorithmic Diversity}
\subtitle{A Mechanism for Distributive Justice in a Socio-Technical MAS}
\author{
	\textbf{Vivek Nallur}, Eamonn O'Toole, Nicol\'{a}s Cardozo and Siobh\'{a}n Clarke\\
	{\it Trinity College Dublin}\\
}
\date{
	\begin{tikzpicture}[decoration=Koch curve type 2] 
		\draw[DeepSkyBlue4] decorate{ decorate{ decorate{ (0,0) -- (3,0) }}}; 
	\end{tikzpicture}  
	\\
	\vspace{0.5cm}
	\today
}
\titlepage
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\begin{frame}{}
%\tableofcontents
%\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{\scshape Context / Background}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{frame}{Computational Social Choice}
\begin{block}
{Design and analysis of methods that result in choices being made through collective action}
{
\begin{enumerate}
	\item Preference Aggregation
	\item Voting
	\item Fair division of resources (distributive justice)
	\item Coalition Formation
	\item Judgement 
\end{enumerate}
}
\end{block}
\end{frame}

%\begin{frame}{Multi-Agent Systems (MAS)}
%\begin{block}
%{Individual / Agent-based Modelling}
%\begin{enumerate}
%	\item Often used for simulating complex individual behaviour
%	\item Predict aggregate outcomes
%	\item Resource allocation, market-dynamics, behaviour of equilibrium under various starting conditions, etc.
%\end{enumerate}
%\end{block}
%\begin{block}{Resource Allocation}
%		\begin{itemize}
%			\item Pareto-Efficiency
%			\item Envy-Freeness
%		\end{itemize}
%\end{block}
%\end{frame}
\begin{frame}{Distributive Justice}
\begin{block}{Based on the notion of \emph{Legitimate Claims}\footnote{\textbf{N. Rescher}. Distributive Justice: A Constructive Critique of the Utilitarian Theory of Distribution. The Bobbs-Merrill Co. Inc., New York, 1966.}}
\begin{enumerate}
	\item More nuanced than \textit{Envy-Freeness}
	\item \textit{Utility theory} is not sufficient to capture this notion
	\item Example: A route-choice problem through a city (pick road A or road B to reach destination)
	\begin{itemize}
		\item Formulation as a traffic-flow optimization is inadequate
		\item Yield a satisfactory outcome to \textbf{each} agent, at least \textit{some} of the time
	\end{itemize}
\end{enumerate}
\end{block}
\begin{figure}
\includegraphics[scale=0.5]{figures/minority_route.jpg}
\end{figure}
\end{frame}


\begin{frame}{Algorithm Diversity}
\begin{block}{Simultaneous use of:}
\begin{enumerate}
	\item Different pathways through the same algorithm
	\item Multiple algorithms in the same running system
\end{enumerate}
\end{block}
\begin{block}{Types of Algorithmic Diversity}
\begin{enumerate}
	\item Induced Diversity, \textit{a.k.a}, Parametric Diversity
	\item Natural Diversity, \textit{a.k.a}, Strategy Diversity
\end{enumerate}
\end{block}
\end{frame}


\begin{frame}{Hypothesis}
\begin{block}
{Introducing algorithm diversity can lead to distributive justice, without centralized oversight of the game or the agents}
\end{block}
\end{frame}

\section{\scshape Experiment}
\begin{frame}{Extremely Simple Socio-Technical MAS}

\medskip
\begin{columns}
	\column{.5\textwidth}
		\includegraphics[scale=0.5]{figures/what-is-minority.jpeg}\\
		\includegraphics[scale=0.5]{figures/minority_wins.png}
	
	\column{.5\textwidth}
	\begin{itemize}
		\item Population consists of \texttt{N} agents (N = odd)
		\item Agents choose between one of two actions (say, \texttt{+1} and \texttt{-1})
		\item Minority wins!
	\end{itemize}
\end{columns}
\end{frame}

\begin{frame}
{That's it!}
\begin{block}
{Stylized model of many real-life scenarios: Market-Entry Games, Traffic Route Choices, Packet-Traffic in Networks, Ecologies of Foraging Animals...}
\end{block}

\begin{block}
{Repeated normal form game}
\begin{enumerate}
	\item The more efficient the system, lesser the difference between majority and minority; 
	\item Provably no Nash equilibrium that satisfies all agents, if \textit{even two} have a mixed strategy\footnote{Learning with fixed rules: The Minority Game. \textit{Willemien Kets}. Journal of Economic Surveys, 26:5, 865--878, December 2012}
\end{enumerate}
\end{block}

\begin{block}
{How should you play?}
\end{block}
\end{frame}

\begin{frame}
{Using a Known Good Strategy: BestPlay}
\medskip
\begin{columns}
	\column{.5\textwidth}
		\includegraphics[scale=0.7]{figures/minority-game-m-s.png}
		
	\column{.5\textwidth}
		\begin{itemize}
			\item Keep a memory (\texttt{m}) of the game rounds
			\item Keep a set of strategies (\texttt{s}) that map \texttt{m} to next action
			\item All winning strategies get rewards; losing strategies get no rewards
			\item Use the `best' strategy to play next move
		\end{itemize}
\end{columns}
\end{frame}

\begin{frame}{BestPlay}
\begin{table}[ht]
\centering
\begin{tabular}{|l|r|}
\hline
\textbf{Strategy} & \textbf{Output} \\ \hline
{[}+1 -1 +1{]}    & -1              \\ \hline
{[}-1 -1 -1{]}    & -1              \\ \hline
{[}-1 -1 +1{]}    & +1              \\ \hline
{[}+1 +1 +1{]}    & -1                \\ \hline
\end{tabular}
\caption{Sample Strategy Pool for a Player}
\label{mg_sample_strategies}
\end{table}
\begin{block}{Important Variables}
\begin{itemize}
	\item \texttt{m} --- Size of memory
	\item \texttt{s} --- Size of strategy pool
	\begin{itemize}
		\item \texttt{s} grows hyper-exponentially ($2^{2^{m}}$) with \texttt{m}, so typically chosen randomly \footnote{The Minority Game: An Introductory Guide. \textit{Esteban Moro}. Advances in Condensed Matter and Statistical Physics. 2004}
	\end{itemize}
\end{itemize}
\end{block}
\end{frame}

\begin{frame}{Add Strategies}
\begin{block}{Evolutionary Strategy}
\begin{enumerate}
	\item Use BestPlay strategy as a genome
	\item Every reproduction cycle, select 10 worst performing agents and 10 best performing agents
	\item For each of the worst performing set: substitute strategies from 2 random best-performing agents
	\item Effectively, 2 best-performing agents have reproduced to replace one from the worst-performing set
	\item Mutate according to the agent's own (very small) mutation probability
\end{enumerate}
\end{block}

\begin{block}{Reinforcement Learning}
\begin{enumerate}
	\item Each agent implements the same well-known Reinforcement Learning algorithm (Roth-Erev)
\end{enumerate}
\end{block}

\end{frame}

\begin{frame}{Experimental Setup: Constant Factors}
\begin{table}[ht!]
\centering
%\large
\def\arraystretch{1.5}
\begin{tabular}{|l|c|}
%\toprule
\hline
{\bf Variable}       & {\bf Value} \\ \hline
Population Size      & 501         \\ \hline
Simulation Period (rounds)    & 2000        \\ \hline
Repetition of Variation		   & 100  \\ \hline
\end{tabular}
%\caption{Experimental constants}
\label{experimental_constants}
\end{table}
\begin{block}
{Diversity measurement using \emph{Shannon Index}}
\[
 H' = -\sum_{i=1}^R p_i \ln p_i
\]
\end{block}
\end{frame}

\begin{frame}{Experimental Setup: Variables Measured}
\begin{block}
{Distribution of rewards amongst population}
{The \emph{fatter} the distribution, the \emph{fairer} the result}
\end{block}
\vspace{1cm}

\begin{block}
{Level of median reward}
{The \emph{higher} the median, the \emph{better} the result}
\end{block}
\end{frame}

\begin{frame}{Experimental Setup: Diversification}
\begin{block}
{Parametric Diversity}
\begin{itemize}
	\item Diversity in the initialization parameters
	\item That is, all agents implement the same algorithm, but use different parameters
\end{itemize}
\end{block}

\begin{block}
{Strategy Diversity}
\begin{itemize}
	\item Diversity in strategy used
	\item That is, different proportions of agents use different strategies (amongst three given before)
\end{itemize}
\end{block}

\end{frame}

\section{\scshape Results}
\begin{frame}{Parameter Diversification - Best Play}
	\begin{block}
	{Parameter Diversified: \texttt{s}}
	\end{block}
	\begin{figure}
		\includegraphics[scale=0.35, clip, trim=0 0 3cm 1cm]{graphs/param-diversity/bestPlay.pdf}
	\end{figure}
\end{frame}

\begin{frame}{Parameter Diversification - Evolutionary}
	\begin{block}
	{Parameter Diversified: \texttt{Reproduction cycle}}
	\end{block}
	\begin{figure}
		\includegraphics[scale=0.35, clip, trim=0 0 3.5cm 1cm]{graphs/param-diversity/evo.pdf}
	\end{figure}
\end{frame}

\begin{frame}{Parameter Diversification - Roth-Erev}
	\begin{block}
	{Parameter Diversified: \texttt{Recency, Epsilon}}
	\end{block}
	\begin{figure}
		\includegraphics[scale=0.35, clip, trim=0 0 3.5cm 1cm]{graphs/param-diversity/rother.pdf}
	\end{figure}
\end{frame}


\begin{frame}{Strategy Diversification - BestPlay and Evo}
	\begin{figure}
		\includegraphics[scale=0.35, clip, trim=0 0 3cm 1cm]{graphs/strategy-diversity/evoBest.pdf}
	\end{figure}
\end{frame}

\begin{frame}{Strategy Diversification - BestPlay and Roth-Erev}

	\begin{figure}
		\includegraphics[scale=0.35, clip, trim=0 0 3.5cm 1cm]{graphs/strategy-diversity/rl_Best_minmax.pdf}
	\end{figure}
\end{frame}

\begin{frame}{Strategy Diversification - Evo and Roth-Erev}

	\begin{figure}
		\includegraphics[scale=0.35, clip, trim=0 0 3.5cm 1cm]{graphs/strategy-diversity/evoRother.pdf}
	\end{figure}
\end{frame}

\begin{frame}{Strategy Diversification - All three}
	\begin{figure}
		\includegraphics[scale=0.35, clip, trim=0 0 3.5cm 1cm]{graphs/strategy-diversity/evoRlBestPlay.pdf}
	\end{figure}
\end{frame}

%\begin{frame}{Results: Parameter Diversity}
%\begin{figure}
%\centering
%\begin{minipage}{0.45\textwidth}
%\centering
%%\includegraphics[scale=0.34]{graphs/Each100Iter-Distance.png}
%\caption{One-to-All}
%\end{minipage}\hfill
%\begin{minipage}{0.45\textwidth}
%\centering
%%\includegraphics[scale=0.34]{graphs/100IterAll-Distance.png}
%\caption{All-to-One}
%\end{minipage}
%\end{figure}
%\end{frame}
%
%\begin{frame}{Results: Strategy Diversity}
%
%\end{frame}

\begin{frame}{All together now}
\begin{figure}
\centering
\includegraphics[scale=0.45, clip, trim=0 0 3cm 0]{graphs/strategy-diversity/allTogether.pdf}
\end{figure}
\end{frame}

\begin{frame}{Show me the numbers}
\renewcommand{\arraystretch}{1.0}
\begin{table}[]
%\centering
\begin{tabular}{llll}
\hline
\multicolumn{1}{|l|}{\textbf{Strategy}}                & \multicolumn{1}{l|}{\textbf{Lowest}} & \multicolumn{1}{l|}{\textbf{Median}} & \multicolumn{1}{l|}{\textbf{Highest}} \\ \hline
\multicolumn{4}{l}{\textit{Pure}}                                                                                                                                                                 \\ \hline
\multicolumn{1}{|l|}{BestPlay}                         & \multicolumn{1}{l|}{368}                    & \multicolumn{1}{l|}{483}                    & \multicolumn{1}{l|}{1001}                    \\ \hline
\multicolumn{1}{|l|}{Evo}                              & \multicolumn{1}{l|}{460}                    & \multicolumn{1}{l|}{680}                    & \multicolumn{1}{l|}{1003}                    \\ \hline
\multicolumn{1}{|l|}{Roth-Erev}                        & \multicolumn{1}{l|}{666}                    & \multicolumn{1}{l|}{929}                    & \multicolumn{1}{l|}{1035}                    \\ \hline
\multicolumn{4}{l}{\textit{Combination}}                                                                                                                                                          \\ \hline
\multicolumn{1}{|l|}{Evo-BestPlay (d = 0.86)}          & \multicolumn{1}{l|}{263}                    & \multicolumn{1}{l|}{509}                    & \multicolumn{1}{l|}{1001}                    \\ \hline
\multicolumn{1}{|l|}{Evo-BestPlay (d = 2.60)}          & \multicolumn{1}{l|}{340}                    & \multicolumn{1}{l|}{733}                    & \multicolumn{1}{l|}{1002}                    \\ \hline
\multicolumn{1}{|l|}{RothErev-BestPlay (d = 1.09)}     & \multicolumn{1}{l|}{371}                    & \multicolumn{1}{l|}{499}                    & \multicolumn{1}{l|}{1030}                    \\ \hline
\multicolumn{1}{|l|}{RothErev-BestPlay (d = 5.03)}     & \multicolumn{1}{l|}{362}                    & \multicolumn{1}{l|}{939}                    & \multicolumn{1}{l|}{1112}                    \\ \hline
\multicolumn{1}{|l|}{Evo-RothErev (d = 2.97)}          & \multicolumn{1}{l|}{734}                    & \multicolumn{1}{l|}{883}                    & \multicolumn{1}{l|}{1004}                    \\ \hline
\multicolumn{1}{|l|}{\textbf{Evo-RothErev (d = 5.13)}} & \multicolumn{1}{l|}{\textbf{774}}           & \multicolumn{1}{l|}{\textbf{957}}           & \multicolumn{1}{l|}{\textbf{1046}}           \\ \hline
\multicolumn{1}{|l|}{BestPlay-Evo-RothErev (d = 1.36)} & \multicolumn{1}{l|}{260}                    & \multicolumn{1}{l|}{526}                    & \multicolumn{1}{l|}{1053}                    \\ \hline
\multicolumn{1}{|l|}{BestPlay-Evo-RothErev (d = 3.51)} & \multicolumn{1}{l|}{297}                    & \multicolumn{1}{l|}{905}                    & \multicolumn{1}{l|}{1048}                    \\ \hline
\end{tabular}
%\caption{Various levels of diversity compared to `pure play' of single strategies}
%\label{tbl_diversity_payoffs_compared}
\end{table}

\end{frame}


\section{\scshape Conclusion}
\begin{frame}{What can we conclude?}
\begin{block}{}
The effect of diversity is to push the population, as a whole, towards a more even distribution of rewards, without needing a centralized guiding hand
\end{block}
\end{frame}


\begin{frame}{That's all, folks!}
	Questions? Comments? Suggestions?
\end{frame}

\end{document}